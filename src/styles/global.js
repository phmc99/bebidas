import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    font-family: 'Noto Sans', sans-serif;
    background-color: #F7F6EE;
    color: #212322;
  }

  h1, h2 {
    font-family: 'Mulish', sans-serif;
  }

`;
