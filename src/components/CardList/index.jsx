import { useContext } from "react";
import { CatalogueContext } from "../../providers/catalogue";
import { GraduationContext } from "../../providers/graduation";
import { PartyContext } from "../../providers/party";
import { WeddingContext } from "../../providers/wedding";
import Button from "../Button";
import DrinkCard from "../DrinkCard";
import { Box } from "../CardList/style";
import { List } from "./style";

const CardList = ({ type }) => {
  const { catalogue, handleSeeMore, buttonText } = useContext(CatalogueContext);
  const { wedding } = useContext(WeddingContext);
  const { party } = useContext(PartyContext);
  const { graduation } = useContext(GraduationContext);

  let listType;

  if (type === "wedding") {
    listType = wedding;
  }
  if (type === "party") {
    listType = party;
  }
  if (type === "graduation") {
    listType = graduation;
  }
  if (type === "catalogue") {
    listType = catalogue;
  }
  if (type === undefined || type.trim() === "") {
    listType = [];
  }

  return (
    <Box>
      {listType.length > 0 ? (
        <>
          <List>
            {listType.map((item, index) => (
              <li key={index}>
                <DrinkCard
                  title={item.name}
                  description={item.description}
                  image={item["image_url"]}
                  litres={item.volume.value}
                  year={item["first_brewed"]}
                  type={type}
                  item={item}
                />
              </li>
            ))}
          </List>
          {type === "catalogue" && (
            <Button func={handleSeeMore}>{buttonText}</Button>
          )}
        </>
      ) : (
        <h1>Nothing to show</h1>
      )}
    </Box>
  );
};

export default CardList;
