import styled from "styled-components";

export const Box = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 20px;
  width: 90%;
  margin: 0 auto;
`;

export const List = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 12px;

  width: 90%;
  margin: 0 auto;

  padding: 10px;
  list-style: none;

  li {
    width: 280px;
    height: 420px;
    padding: 3px;
    border-radius: 5px;
  }
`;
