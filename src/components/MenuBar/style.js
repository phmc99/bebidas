import styled from "styled-components";

export const Menu = styled.div`
  width: 100%;
  height: 60px;
  background-color: #212322;

  .toggle-menu {
    color: #f7f6ee;
    position: absolute;
    top: 15px;
    left: 85%;
    display: flex;
    font-size: 30px;

    @media (min-width: 768px) {
      display: none;
    }
  }
`;

export const ToolBar = styled.div`
  background-color: #212322;
  padding: 10px 20px;

  position: fixed;
  width: 100vw;
  height: 100vh;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  display: ${(props) => (props.toggle ? "flex" : "none")};
  flex-direction: column;

  @media (min-width: 768px) {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    height: 100%;

    position: static;
    top: 0;
    left: 0;
    transform: translate(0);
  }
`;

export const MenuList = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 10px;
  height: 100%;
  list-style: none;

  li {
    cursor: pointer;
    width: 100px;
    border-bottom: 1px solid #f7f6ee;
  }

  @media (min-width: 768px) {
    flex-direction: row;
    float: right;

    li {
      &:hover {
        border-color: #81aeff;
      }
    }
  }
`;

export const MenuItem = styled.div`
  text-align: center;
  padding: 10px;
  color: #f7f6ee;

  &:hover {
    color: #81aeff;
  }
`;

export const HomeButton = styled.div`
  font-size: 32px;
  color: #f7f6ee;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 768px) {
    height: 100%;
    cursor: pointer;
    float: left;
    &:hover {
      color: #81aeff;
    }
  }
`;
