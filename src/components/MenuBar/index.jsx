import { useHistory } from "react-router-dom";
import { HomeButton, Menu, MenuItem, MenuList, ToolBar } from "./style";
import { IoIosClose, IoIosHome, IoMdMenu } from "react-icons/io";
import { useState } from "react";

const MenuBar = () => {
  const [toggle, setToggle] = useState(false);

  const history = useHistory();

  const changePage = (page) => {
    history.push(`/${page}`);
    setToggle(!toggle);
  };

  return (
    <Menu>
      <ToolBar toggle={toggle}>
        <HomeButton onClick={() => changePage("")}>
          <IoIosHome />
        </HomeButton>

        <MenuList>
          <li>
            <MenuItem onClick={() => changePage("wedding")}>Wedding</MenuItem>
          </li>
          <li>
            <MenuItem onClick={() => changePage("party")}>Party</MenuItem>
          </li>
          <li>
            <MenuItem onClick={() => changePage("graduation")}>
              Graduation
            </MenuItem>
          </li>
        </MenuList>
      </ToolBar>

      <div className="toggle-menu" onClick={() => setToggle(!toggle)}>
        {toggle ? <IoIosClose /> : <IoMdMenu />}
      </div>
    </Menu>
  );
};

export default MenuBar;
