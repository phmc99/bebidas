import { Btn } from "./style";

const Button = ({ func, children }) => {
  return <Btn onClick={func}>{children}</Btn>;
};

export default Button;
