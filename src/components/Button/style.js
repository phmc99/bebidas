import styled from "styled-components";

export const Btn = styled.button`
  border: 0;
  padding: 5px;
  cursor: pointer;
  background-color: rgba(129, 174, 255, 1);
  border-radius: 5px;

  width: 80px;
  font-size: 16px;
  margin: 10px 0;

  text-align: center;
  align-self: center;

  &:hover {
    filter: brightness(1.25);
  }
`;
