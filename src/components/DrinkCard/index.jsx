import { BoxCard } from "./style";
import Button from "../Button";
import { GiBigDiamondRing, GiGraduateCap, GiPartyPopper } from "react-icons/gi";
import { useContext } from "react";
import { GraduationContext } from "../../providers/graduation";
import { PartyContext } from "../../providers/party";
import { WeddingContext } from "../../providers/wedding";

const DrinkCard = ({ title, description, image, litres, year, type, item }) => {
  const { addToGraduation, removeFromGraduation } =
    useContext(GraduationContext);
  const { addToParty, removeFromParty } = useContext(PartyContext);
  const { addToWedding, removeFromWedding } = useContext(WeddingContext);

  const handleRemove = (item) => {
    if (type === "wedding") {
      return removeFromWedding(item);
    }
    if (type === "party") {
      return removeFromParty(item);
    }
    if (type === "graduation") {
      return removeFromGraduation(item);
    }
    if (type === undefined || type.trim() === "") {
      return;
    }
  };

  return (
    <BoxCard>
      <div className="title">
        <h1>{title}</h1>
      </div>

      <figure>
        <img src={image} alt={title} />
      </figure>

      <aside>
        {type === "catalogue" && <span>Add this drink in a list</span>}

        <div className="button-list">
          {type === "catalogue" ? (
            <>
              <Button func={() => addToWedding(item)}>
                <GiBigDiamondRing />
              </Button>
              <Button func={() => addToParty(item)}>
                <GiPartyPopper />
              </Button>
              <Button func={() => addToGraduation(item)}>
                <GiGraduateCap />
              </Button>
            </>
          ) : (
            <Button func={() => handleRemove(item)}>Remove</Button>
          )}
        </div>
        <div className="info">
          <span>Litres:&nbsp;{litres}</span>
          <span>Fabrication:&nbsp;{year}</span>
        </div>
        <p>{description}</p>
      </aside>
    </BoxCard>
  );
};

export default DrinkCard;
