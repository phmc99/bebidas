import styled from "styled-components";

export const BoxCard = styled.div`
  background-color: #f3f3f3;
  padding: 10px;
  height: 100%;
  border-radius: 5px;
  box-shadow: 0px 0px 5px 1px #00000082;

  display: flex;
  flex-direction: column;
  align-items: center;

  .title {
    width: 85%;
    height: 50px;

    padding: 10px;
    margin: 10px 0;
    text-align: center;

    h1 {
      font-size: 18px;
      height: 45px;
    }
  }

  figure {
    height: 180px;
    img {
      width: 80px;
      height: 180px;
    }
  }

  span {
    font-size: 12px;
  }
  .button-list {
    display: flex;
    gap: 5px;
    justify-content: center;
  }

  aside {
    text-align: center;
    margin-top: 10px;
    overflow: scroll;

    ::-webkit-scrollbar {
      display: none;
    }

    .info {
      display: flex;
      justify-content: space-between;
      margin: 10px 0;
    }

    p {
      text-align: left;
      font-size: 14px;
    }
  }
`;
