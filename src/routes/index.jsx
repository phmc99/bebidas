import React from "react";
import { Route, Switch } from "react-router-dom";
import GraduationPage from "../pages/GraduationPage";
import HomePage from "../pages/Home";
import PartyPage from "../pages/PartyPage";
import WeddingPage from "../pages/WeddingPage";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <HomePage />
      </Route>
      <Route path="/graduation">
        <GraduationPage />
      </Route>
      <Route path="/party">
        <PartyPage />
      </Route>
      <Route path="/wedding">
        <WeddingPage />
      </Route>
    </Switch>
  );
};

export default Routes;
