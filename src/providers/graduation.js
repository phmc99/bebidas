import { createContext, useState } from "react";
import toast from "react-hot-toast";

export const GraduationContext = createContext([]);

export const GraduationProvider = ({ children }) => {
  const [graduation, setGraduation] = useState([]);

  const addToGraduation = (item) => {
    setGraduation([...graduation, item]);
    toast.success("Item adicionado no carrinho");
  };

  const removeFromGraduation = (item) => {
    const newGraduation = graduation.filter(
      (itemOnGraduation) => itemOnGraduation.name !== item.name
    );
    setGraduation(newGraduation);
    toast.error("Item removido do carrinho");
  };

  return (
    <GraduationContext.Provider
      value={{ graduation, addToGraduation, removeFromGraduation }}
    >
      {children}
    </GraduationContext.Provider>
  );
};
