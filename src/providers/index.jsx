import { CatalogueProvider } from "./catalogue";
import { GraduationProvider } from "./graduation";
import { PartyProvider } from "./party";
import { WeddingProvider } from "./wedding";

const Providers = ({ children }) => {
  return (
    <CatalogueProvider>
      <GraduationProvider>
        <PartyProvider>
          <WeddingProvider>{children}</WeddingProvider>
        </PartyProvider>
      </GraduationProvider>
    </CatalogueProvider>
  );
};

export default Providers;
