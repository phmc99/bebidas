import { createContext, useState } from "react";
import toast from "react-hot-toast";

export const PartyContext = createContext([]);

export const PartyProvider = ({ children }) => {
  const [party, setParty] = useState([]);

  const addToParty = (item) => {
    setParty([...party, item]);
    toast.success("Item adicionado no carrinho");
  };

  const removeFromParty = (item) => {
    const newParty = party.filter(
      (itemOnParty) => itemOnParty.name !== item.name
    );
    setParty(newParty);
    toast.error("Item removido do carrinho");
  };

  return (
    <PartyContext.Provider value={{ party, addToParty, removeFromParty }}>
      {children}
    </PartyContext.Provider>
  );
};
