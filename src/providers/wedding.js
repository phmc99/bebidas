import { createContext, useState } from "react";
import toast from "react-hot-toast";

export const WeddingContext = createContext([]);

export const WeddingProvider = ({ children }) => {
  const [wedding, setWedding] = useState([]);

  const addToWedding = (item) => {
    setWedding([...wedding, item]);
    toast.success("Item adicionado no carrinho");
  };

  const removeFromWedding = (item) => {
    const newWedding = wedding.filter(
      (itemOnWedding) => itemOnWedding.name !== item.name
    );
    setWedding(newWedding);
    toast.error("Item removido do carrinho");
  };

  return (
    <WeddingContext.Provider
      value={{ wedding, addToWedding, removeFromWedding }}
    >
      {children}
    </WeddingContext.Provider>
  );
};
