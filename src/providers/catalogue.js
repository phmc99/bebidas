import axios from "axios";
import { createContext, useState, useEffect } from "react";

export const CatalogueContext = createContext([]);

export const CatalogueProvider = ({ children }) => {
  const [catalogue, setCatalogue] = useState([]);
  const [page, setPage] = useState(1);
  const [buttonText, setButtonText] = useState("See more");

  useEffect(() => {
    axios.get(`https://api.punkapi.com/v2/beers?page=1`).then((response) => {
      setCatalogue(response.data);
    });
  }, []);

  const handleSeeMore = async () => {
    setPage(page + 1);

    await axios
      .get(`https://api.punkapi.com/v2/beers?page=${page}`)
      .then((response) => {
        if (page <= 12) {
          setButtonText("See more");
          setCatalogue([...catalogue, ...response.data]);
        }
      });
  };

  return (
    <CatalogueContext.Provider value={{ catalogue, handleSeeMore, buttonText }}>
      {children}
    </CatalogueContext.Provider>
  );
};
