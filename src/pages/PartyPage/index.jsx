import CardList from "../../components/CardList";
import MenuBar from "../../components/MenuBar";

const PartyPage = () => {
  return (
    <div>
      <MenuBar />
      <CardList type="party" />
    </div>
  );
};

export default PartyPage;
