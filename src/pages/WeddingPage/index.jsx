import CardList from "../../components/CardList";
import MenuBar from "../../components/MenuBar";

const WeddingPage = () => {
  return (
    <div>
      <MenuBar />
      <CardList type="wedding" />
    </div>
  );
};

export default WeddingPage;
