import CardList from "../../components/CardList";
import MenuBar from "../../components/MenuBar";

const GraduationPage = () => {
  return (
    <div>
      <MenuBar />
      <CardList type="graduation" />
    </div>
  );
};

export default GraduationPage;
