import CardList from "../../components/CardList";
import MenuBar from "../../components/MenuBar";

const HomePage = () => {
  return (
    <div>
      <MenuBar />
      <CardList type="catalogue" />
    </div>
  );
};

export default HomePage;
